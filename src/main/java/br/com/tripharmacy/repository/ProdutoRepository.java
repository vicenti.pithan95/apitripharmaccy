package br.com.tripharmacy.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import br.com.tripharmacy.modelo.Produto;

public interface ProdutoRepository extends JpaRepository<Produto,Integer> {

}

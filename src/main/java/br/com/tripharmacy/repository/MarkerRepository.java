package br.com.tripharmacy.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.tripharmacy.modelo.Marker;

public interface MarkerRepository extends JpaRepository<Marker,Integer> {
	
	@Query("select m from Marker m where m.idMarker = :idMarker")
	Optional<Marker> findByIdMarker(@Param("idMarker") int marker);
	


	
}

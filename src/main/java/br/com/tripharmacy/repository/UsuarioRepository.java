package br.com.tripharmacy.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.tripharmacy.modelo.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario ,Long> {
	
	Optional<Usuario> findByDsLogin(String dsLogin);
}

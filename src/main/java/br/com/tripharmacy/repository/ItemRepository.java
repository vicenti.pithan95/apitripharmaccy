package br.com.tripharmacy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.tripharmacy.modelo.ItemPedido;

public interface ItemRepository extends JpaRepository<ItemPedido, Integer>{

}

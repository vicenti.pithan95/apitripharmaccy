package br.com.tripharmacy.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.tripharmacy.modelo.Farmacia;


public interface FarmaciaRepository extends JpaRepository<Farmacia,Integer> {
	
	Farmacia findByidFarmacia(int idFarmacia);

	Optional<Farmacia> findByNoCnpj(String noCnpj);
	

	
}

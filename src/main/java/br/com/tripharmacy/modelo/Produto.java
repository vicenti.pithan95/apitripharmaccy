package br.com.tripharmacy.modelo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Produto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idProduto;
	@ManyToOne
	@JoinColumn(name = "id_farmacia")
	private Farmacia farmacia;
	private String dsProduto;
	private int dsQuantidadeDisp;
	private Date dtInc;
	private Date dtAlt;
	private double vlUnitario;
	private String dsTipo;
	private String dsPeso;
	private int qtUnidades;
	private String dsClassificacao;
	private boolean icPrescricaoMedica;
	private String dsBula;

	public Produto(Farmacia farmacia, String dsProduto, int dsQuantidadeDisp, double vlUnitario, String dsTipo,
			String dsPeso, int qtUnidades, String dsClassificacao, boolean icPrescricaoMedica, String dsBula) {
		this.farmacia = farmacia;
		this.dsProduto = dsProduto;
		this.dsQuantidadeDisp = dsQuantidadeDisp;
		this.dtInc = new Date(System.currentTimeMillis());
		;
		this.vlUnitario = vlUnitario;
		this.dsTipo = dsTipo;
		this.dsPeso = dsPeso;
		this.qtUnidades = qtUnidades;
		this.dsClassificacao = dsClassificacao;
		this.icPrescricaoMedica = icPrescricaoMedica;
		this.dsBula = dsBula;
	}

	public Produto(){
		
	}

	public int getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}

	public Farmacia getFarmacia() {
		return farmacia;
	}

	public void setFarmacia(Farmacia farmacia) {
		this.farmacia = farmacia;
	}

	public String getDsProduto() {
		return dsProduto;
	}

	public void setDsProduto(String dsProduto) {
		this.dsProduto = dsProduto;
	}

	public int getDsQuantidadeDisp() {
		return dsQuantidadeDisp;
	}

	public void setDsQuantidadeDisp(int dsQuantidadeDisp) {
		this.dsQuantidadeDisp = dsQuantidadeDisp;
	}

	public Date getDtInc() {
		return dtInc;
	}

	public void setDtInc(Date dtInc) {
		this.dtInc = dtInc;
	}

	public Date getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(Date dtAlt) {
		this.dtAlt = dtAlt;
	}

	public double getVlUnitario() {
		return vlUnitario;
	}

	public void setVlUnitario(double vlUnitario) {
		this.vlUnitario = vlUnitario;
	}

	public String getDsTipo() {
		return dsTipo;
	}

	public void setDsTipo(String dsTipo) {
		this.dsTipo = dsTipo;
	}

	public String getDsPeso() {
		return dsPeso;
	}

	public void setDsPeso(String dsPeso) {
		this.dsPeso = dsPeso;
	}

	public int getQtUnidades() {
		return qtUnidades;
	}

	public void setQtUnidades(int qtUnidades) {
		this.qtUnidades = qtUnidades;
	}

	public String getDsClassificacao() {
		return dsClassificacao;
	}

	public void setDsClassificacao(String dsClassificacao) {
		this.dsClassificacao = dsClassificacao;
	}

	public boolean isIcPrescricaoMedica() {
		return icPrescricaoMedica;
	}

	public void setIcPrescricaoMedica(boolean icPrescricaoMedica) {
		this.icPrescricaoMedica = icPrescricaoMedica;
	}

	public String getDsBula() {
		return dsBula;
	}

	public void setDsBula(String dsBula) {
		this.dsBula = dsBula;
	}

}

package br.com.tripharmacy.modelo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import resources.TipoPessoa;

@Entity
public class Usuario implements UserDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUsuario;
	
	private String dsNome;
	
	private String dsLogin;
	
	private String tpUsuario;
	
	private String dsSenha;
	
	private String tpPessoa;
	
	private String noCpfCnpj;
	
	private String noTelefoneCel;
	
	private String noTelefoneRes;
	
	private String noTelefoneAux;
	
	private String dsEmail;
	
	private Date dtInc;
	
	private Date dtAlt;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Perfil> perfis = new ArrayList<>();
	
	

	public Usuario() {
		
	}



	
	public Usuario(Long idUsuario, String dsNome, String dsLogin, String tpUsuario, String dsSenha, TipoPessoa tpPessoa,
			String noCpfCnpj, String noTelefoneCel, String noTelefoneRes, String noTelefoneAux, String dsEmail,
			Date dtInc) {
		this.idUsuario = idUsuario;
		this.dsNome = dsNome;
		this.dsLogin = dsLogin;
		this.tpUsuario = tpUsuario;
		this.dsSenha = dsSenha;
		this.tpPessoa = tpPessoa.toString();
		this.noCpfCnpj = noCpfCnpj;
		this.noTelefoneCel = noTelefoneCel;
		this.noTelefoneRes = noTelefoneRes;
		this.noTelefoneAux = noTelefoneAux;
		this.dsEmail = dsEmail;
		this.dtInc = new Date(System.currentTimeMillis());
		
	}

	public String getTpPessoa() {
		return tpPessoa;
	}

	public void setTpPessoa(String tpPessoa) {
		this.tpPessoa = tpPessoa;
	}

	public String getNoCpfCnpj() {
		return noCpfCnpj;
	}

	public void setNoCpfCnpj(String noCpfCnpj) {
		this.noCpfCnpj = noCpfCnpj;
	}

	public String getNoTelefoneCel() {
		return noTelefoneCel;
	}

	public void setNoTelefoneCel(String noTelefoneCel) {
		this.noTelefoneCel = noTelefoneCel;
	}

	public String getNoTelefoneRes() {
		return noTelefoneRes;
	}

	public void setNoTelefoneRes(String noTelefoneRes) {
		this.noTelefoneRes = noTelefoneRes;
	}

	public String getNoTelefoneAux() {
		return noTelefoneAux;
	}

	public void setNoTelefoneAux(String noTelefoneAux) {
		this.noTelefoneAux = noTelefoneAux;
	}

	public String getDsEmail() {
		return dsEmail;
	}

	public void setDsEmail(String dsEmail) {
		this.dsEmail = dsEmail;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getDsNome() {
		return dsNome;
	}

	public void setDsNome(String dsNome) {
		this.dsNome = dsNome;
	}

	public String getDsLogin() {
		return dsLogin;
	}

	public void setDsLogin(String dsLogin) {
		this.dsLogin = dsLogin;
	}

	public String getTpUsuario() {
		return tpUsuario;
	}

	public void setTpUsuario(String tpUsuario) {
		this.tpUsuario = tpUsuario;
	}

	public String getDsSenha() {
		return dsSenha;
	}

	public void setDsSenha(String dsSenha) {
		this.dsSenha = dsSenha;
	}

	public Date getDtInc() {
		return dtInc;
	}

	public void setDtInc(Date dtInc) {
		this.dtInc = dtInc;
	}

	public Date getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(Date dtAlt) {
		this.dtAlt = dtAlt;
	}




	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.perfis;
	}




	@Override
	public String getPassword() {
		return this.dsSenha;
	}




	@Override
	public String getUsername() {
		return this.dsLogin;
	}




	@Override
	public boolean isAccountNonExpired() {
		return true;
	}




	@Override
	public boolean isAccountNonLocked() {
		return true;
	}




	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}




	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}

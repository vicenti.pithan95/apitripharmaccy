package br.com.tripharmacy.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;




@Entity
public class Marker {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idMarker;
	private String title;
	private double lat;
	private double lng;
	private String snippet;
	
	@JsonIgnore
	@OneToOne(mappedBy = "marker")
	private Farmacia farmacia;
	
	public Marker() {
		
	};
	
	
	

	public Marker(String title, double lat, double lng, String snippet, Farmacia farmacia) {
		this.title = title;
		this.lat = lat;
		this.lng = lng;
		this.snippet = snippet;
		this.farmacia = farmacia;
	}




	public Farmacia getFarmacia() {
		return farmacia;
	}

	public void setFarmacia(Farmacia farmacia) {
		this.farmacia = farmacia;
	}

	public int getId() {
		return idMarker;
	}

	public void setId(int id) {
		this.idMarker = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}

}

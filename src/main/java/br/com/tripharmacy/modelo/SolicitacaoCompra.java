package br.com.tripharmacy.modelo;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class SolicitacaoCompra {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idSolicitacao;
	
	@OneToMany(mappedBy ="solicitacaoCompra" ,cascade = CascadeType.ALL)
	private List<ItemPedido> itemPedido;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	
	private Date dtInc;
	
	private Date dtAlt;
	
	private String dsStatus;
	

	public SolicitacaoCompra(int idSolicitacao, List<ItemPedido> itemPedido, Usuario usuario, String dsStatus, int dsQuantidade) {

		this.idSolicitacao = idSolicitacao;
		this.itemPedido = itemPedido;
		this.usuario = usuario;
		this.dsStatus = dsStatus;
		this.dtInc = new Date(System.currentTimeMillis());
	}

	public SolicitacaoCompra() {

	}

	public int getIdSolicitacao() {
		return idSolicitacao;
	}

	public void setIdSolicitacao(int idSolicitacao) {
		this.idSolicitacao = idSolicitacao;
	}

	public List<ItemPedido> getItemPedido() {
		return itemPedido;
	}

	public void setItemPedido(List<ItemPedido> itemPedido) {
		this.itemPedido = itemPedido;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Date getDtInc() {
		return dtInc;
	}

	public void setDtInc(Date dtInc) {
		this.dtInc = dtInc;
	}

	public Date getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(Date dtAlt) {
		this.dtAlt = dtAlt;
	}

	public String getDsStatus() {
		return dsStatus;
	}

	public void setDsStatus(String dsStatus) {
		this.dsStatus = dsStatus;
	}
	

}
package br.com.tripharmacy.modelo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Farmacia {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idFarmacia;
	private String dsFarmacia;
	private String noCnpj;
	private String dsRazaoSocial;
	private Date dtInc;
	private Date dtAlt;

	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_marker", referencedColumnName = "idMarker")
	private Marker marker;

	public Farmacia() {

	}

	public Farmacia(int idFarmacia, Marker marker, String dsFarmacia, String noCnpj, String dsRazaoSocial) {
		
		this.idFarmacia = idFarmacia;
		this.marker = marker;
		this.dsFarmacia = dsFarmacia;
		this.noCnpj = noCnpj;
		this.dsRazaoSocial = dsRazaoSocial;
		this.dtInc = new Date(System.currentTimeMillis()); 
	}
	

	public int getIdFarmacia() {
		return idFarmacia;
	}

	public void setIdFarmacia(int idFarmacia) {
		this.idFarmacia = idFarmacia;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public String getDsFarmacia() {
		return dsFarmacia;
	}

	public void setDsFarmacia(String dsFarmacia) {
		this.dsFarmacia = dsFarmacia;
	}

	public String getNoCnpj() {
		return noCnpj;
	}

	public void setNoCnpj(String noCnpj) {
		this.noCnpj = noCnpj;
	}

	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	public Date getDtInc() {
		return dtInc;
	}

	public void setDtInc(Date dtInc) {
		this.dtInc = dtInc;
	}

	public Date getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(Date dtAlt) {
		this.dtAlt = dtAlt;
	}



}

package br.com.tripharmacy.controller.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.tripharmacy.modelo.Farmacia;
import br.com.tripharmacy.modelo.Marker;

public class MarkerDto {
	private int id;
	private String title;
	private double lat;
	private double lng;
	private String snippet;
	@JsonIgnore
	private Farmacia farmacia;
	
	public static List<MarkerDto> converter(List<Marker> markers) {
		return markers.stream().map(MarkerDto::new).collect(Collectors.toList());
	}
	public MarkerDto(Marker marker) {
		this.id = marker.getId();
		this.title = marker.getTitle();
		this.lat = marker.getLat();
		this.lng = marker.getLng();
		this.snippet = marker.getSnippet();
		this.farmacia = marker.getFarmacia();
	}


	public Farmacia getIdfarmacia() {
		return farmacia;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public double getLat() {
		return lat;
	}

	public double getLng() {
		return lng;
	}

	public String getSnippet() {
		return snippet;
	}


}

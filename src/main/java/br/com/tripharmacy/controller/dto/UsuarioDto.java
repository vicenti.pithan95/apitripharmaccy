package br.com.tripharmacy.controller.dto;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import br.com.tripharmacy.modelo.Usuario;

public class UsuarioDto {
	private Long idUsuario;
	private String dsNome;
	private String dsLogin;
	private String tpUsuario;
	private String dsSenha;
	private String tpPessoa;
	private String noCpfCnpj;
	private String noTelefoneCel;
	private String noTelefoneRes;
	private String noTelefoneAux;
	private String dsEmail;
	private Date dtInc;
	private Date dtAlt;

	public static List<UsuarioDto> converter(List<Usuario> usuarios) {
		return usuarios.stream().map(UsuarioDto::new).collect(Collectors.toList());
	}

	public UsuarioDto(Usuario usuario) {
	this.dsNome = usuario.getDsNome();
	this.idUsuario = usuario.getIdUsuario();
	this.dsSenha = "";
	this.dsLogin = usuario.getDsLogin();
	this.tpUsuario = usuario.getTpUsuario();
	this.tpPessoa = usuario.getTpPessoa();
	this.noCpfCnpj = usuario.getNoCpfCnpj();
	this.noTelefoneCel = usuario.getNoTelefoneCel();
	this.noTelefoneRes = usuario.getNoTelefoneRes();
	this.noTelefoneAux = usuario.getNoTelefoneAux();
	this.dsEmail = usuario.getDsEmail();
	this.dtAlt = usuario.getDtAlt();
	this.dtInc = usuario.getDtInc();
	
	}

	


	public String getTpPessoa() {
		return tpPessoa;
	}

	public void setTpPessoa(String tpPessoa) {
		this.tpPessoa = tpPessoa;
	}

	public String getNoCpfCnpj() {
		return noCpfCnpj;
	}

	public void setNoCpfCnpj(String noCpfCnpj) {
		this.noCpfCnpj = noCpfCnpj;
	}

	public String getNoTelefoneCel() {
		return noTelefoneCel;
	}

	public void setNoTelefoneCel(String noTelefoneCel) {
		this.noTelefoneCel = noTelefoneCel;
	}

	public String getNoTelefoneRes() {
		return noTelefoneRes;
	}

	public void setNoTelefoneRes(String noTelefoneRes) {
		this.noTelefoneRes = noTelefoneRes;
	}

	public String getNoTelefoneAux() {
		return noTelefoneAux;
	}

	public void setNoTelefoneAux(String noTelefoneAux) {
		this.noTelefoneAux = noTelefoneAux;
	}

	public String getDsEmail() {
		return dsEmail;
	}

	public void setDsEmail(String dsEmail) {
		this.dsEmail = dsEmail;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getDsNome() {
		return dsNome;
	}

	public void setDsNome(String dsNome) {
		this.dsNome = dsNome;
	}

	public String getDsLogin() {
		return dsLogin;
	}

	public void setDsLogin(String dsLogin) {
		this.dsLogin = dsLogin;
	}

	public String getTpUsuario() {
		return tpUsuario;
	}

	public void setTpUsuario(String tpUsuario) {
		this.tpUsuario = tpUsuario;
	}

	public String getDsSenha() {
		return dsSenha;
	}

	public void setDsSenha(String dsSenha) {
		this.dsSenha = dsSenha;
	}

	public Date getDtInc() {
		return dtInc;
	}

	public void setDtInc(Date dtInc) {
		this.dtInc = dtInc;
	}

	public Date getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(Date dtAlt) {
		this.dtAlt = dtAlt;
	}

}

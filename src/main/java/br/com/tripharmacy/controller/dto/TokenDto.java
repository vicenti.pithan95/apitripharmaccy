package br.com.tripharmacy.controller.dto;

public class TokenDto {

	private String token;

	private String tipo;

	private Long id;

	public TokenDto(String token, String tipo, Long id) {

		this.token = token;
		this.tipo = tipo;
		this.id = id;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getToken() {
		return token;
	}

	public String getTipo() {
		return tipo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

}

package br.com.tripharmacy.controller.dto;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import br.com.tripharmacy.modelo.ItemPedido;
import br.com.tripharmacy.modelo.SolicitacaoCompra;
import br.com.tripharmacy.modelo.Usuario;

public class SolicitacaoCompraDto {
	
	private int idSolicitacao;
	private List<ItemPedido> itemPedido;
	private Usuario usuario;
	private Date dtInc;
	private Date dtAlt;
	private String dsStatus;
	private int dsQuantidade;

	public static List<SolicitacaoCompraDto> converter(List<SolicitacaoCompra> solicitacoesCompras) {
		return solicitacoesCompras.stream().map(SolicitacaoCompraDto::new).collect(Collectors.toList());
	}

	public SolicitacaoCompraDto(SolicitacaoCompra solicitacaoCompra) {
		this.setItemPedido(solicitacaoCompra.getItemPedido());
		this.usuario = solicitacaoCompra.getUsuario();
		this.idSolicitacao = solicitacaoCompra.getIdSolicitacao();
		this.dtInc = solicitacaoCompra.getDtInc();
		this.dtAlt = solicitacaoCompra.getDtAlt();
		this.dsStatus = solicitacaoCompra.getDsStatus();
	
	}

	public int getIdSolicitacao() {
		return idSolicitacao;
	}

	public void setIdSolicitacao(int idSolicitacao) {
		this.idSolicitacao = idSolicitacao;
	}


	

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Date getDtInc() {
		return dtInc;
	}

	public void setDtInc(Date dtInc) {
		this.dtInc = dtInc;
	}

	public Date getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(Date dtAlt) {
		this.dtAlt = dtAlt;
	}

	public String getDsStatus() {
		return dsStatus;
	}

	public void setDsStatus(String dsStatus) {
		this.dsStatus = dsStatus;
	}

	public int getDsQuantidade() {
		return dsQuantidade;
	}

	public void setDsQuantidade(int dsQuantidade) {
		this.dsQuantidade = dsQuantidade;
	}

	public List<ItemPedido> getItemPedido() {
		return itemPedido;
	}

	public void setItemPedido(List<ItemPedido> itemPedido) {
		this.itemPedido = itemPedido;
	}

}

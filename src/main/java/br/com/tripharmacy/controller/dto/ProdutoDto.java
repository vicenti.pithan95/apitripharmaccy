package br.com.tripharmacy.controller.dto;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import br.com.tripharmacy.modelo.Farmacia;
import br.com.tripharmacy.modelo.Produto;

public class ProdutoDto {
	private int idProduto;
	private Farmacia farmacia;
	private String dsProduto;
	private int dsQuantidadeDisp;
	private Date dtInc;
	private Date dtAlt;
	private double vlUnitario;
	private String dsTipo;
	private String dsPeso;
	private int qtUnidades;
	private String dsClassificacao;
	private boolean icPrescricaoMedica;
	private String dsBula;
	
	public static List<ProdutoDto> converter(List<Produto> produtos) {
		return produtos.stream().map(ProdutoDto::new).collect(Collectors.toList());
	}
	public ProdutoDto(Produto produto) {
		this.idProduto = produto.getIdProduto();
		this.farmacia = produto.getFarmacia();
		this.dsProduto = produto.getDsProduto();
		this.dsQuantidadeDisp = produto.getDsQuantidadeDisp();
		this.dtInc = produto.getDtInc();
		this.dtAlt = produto.getDtAlt();
		this.dsTipo = produto.getDsTipo();
		this.vlUnitario = produto.getVlUnitario();
		this.qtUnidades = produto.getQtUnidades();
		this.dsPeso = produto.getDsPeso();
		this.dsClassificacao = produto.getDsClassificacao();
		this.icPrescricaoMedica = produto.isIcPrescricaoMedica();
		this.dsBula = produto.getDsBula();
		
	}
	
	public int getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}
	public Farmacia getFarmacia() {
		return farmacia;
	}
	public void setFarmacia(Farmacia farmacia) {
		this.farmacia = farmacia;
	}
	public String getDsProduto() {
		return dsProduto;
	}
	public void setDsProduto(String dsProduto) {
		this.dsProduto = dsProduto;
	}
	public int getDsQuantidadeDisp() {
		return dsQuantidadeDisp;
	}
	public void setDsQuantidadeDisp(int dsQuantidadeDisp) {
		this.dsQuantidadeDisp = dsQuantidadeDisp;
	}
	public Date getDtInc() {
		return dtInc;
	}
	public void setDtInc(Date dtInc) {
		this.dtInc = dtInc;
	}
	public Date getDtAlt() {
		return dtAlt;
	}
	public void setDtAlt(Date dtAlt) {
		this.dtAlt = dtAlt;
	}
	public double getVlUnitario() {
		return vlUnitario;
	}
	public void setVlUnitario(double vlUnitario) {
		this.vlUnitario = vlUnitario;
	}
	public String getDsTipo() {
		return dsTipo;
	}
	public void setDsTipo(String dsTipo) {
		this.dsTipo = dsTipo;
	}
	public String getDsPeso() {
		return dsPeso;
	}
	public void setDsPeso(String dsPeso) {
		this.dsPeso = dsPeso;
	}
	public int getQtUnidades() {
		return qtUnidades;
	}
	public void setQtUnidades(int qtUnidades) {
		this.qtUnidades = qtUnidades;
	}
	public String getDsClassificacao() {
		return dsClassificacao;
	}
	public void setDsClassificacao(String dsClassificacao) {
		this.dsClassificacao = dsClassificacao;
	}
	public boolean isIcPrescricaoMedica() {
		return icPrescricaoMedica;
	}
	public void setIcPrescricaoMedica(boolean icPrescricaoMedica) {
		this.icPrescricaoMedica = icPrescricaoMedica;
	}
	public String getDsBula() {
		return dsBula;
	}
	public void setDsBula(String dsBula) {
		this.dsBula = dsBula;
	}
	


}

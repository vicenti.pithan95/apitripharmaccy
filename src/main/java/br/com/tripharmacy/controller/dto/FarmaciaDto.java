package br.com.tripharmacy.controller.dto;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import br.com.tripharmacy.modelo.Farmacia;
import br.com.tripharmacy.modelo.Marker;

public class FarmaciaDto {

	private int idFarmacia;
	private Marker marker;
	private String dsFarmacia;
	private String noCnpj;
	private String dsRazaoSocial;
	private Date dtInc;
	private Date dtAlt;

	public static List<FarmaciaDto> converter(List<Farmacia> farmacias) {
		return farmacias.stream().map(FarmaciaDto::new).collect(Collectors.toList());
	}
	
	public FarmaciaDto(Farmacia farmacia) {
	this.idFarmacia = farmacia.getIdFarmacia();
	this.marker = farmacia.getMarker();
	this.dsFarmacia = farmacia.getDsFarmacia();
	this.noCnpj = farmacia.getNoCnpj();
	this.dsRazaoSocial = farmacia.getDsRazaoSocial();
	this.dtInc = farmacia.getDtInc();
	this.dtAlt = farmacia.getDtAlt();
	
	}

	public int getIdFarmacia() {
		return idFarmacia;
	}

	public void setIdFarmacia(int idFarmacia) {
		this.idFarmacia = idFarmacia;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public String getDsFarmacia() {
		return dsFarmacia;
	}

	public void setDsFarmacia(String dsFarmacia) {
		this.dsFarmacia = dsFarmacia;
	}

	public String getNoCnpj() {
		return noCnpj;
	}

	public void setNoCnpj(String noCnpj) {
		this.noCnpj = noCnpj;
	}

	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}


	public Date getDtInc() {
		return dtInc;
	}

	public void setDtInc(Date dtInc) {
		this.dtInc = dtInc;
	}

	public Date getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(Date dtAlt) {
		this.dtAlt = dtAlt;
	}


}

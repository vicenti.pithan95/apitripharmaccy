package br.com.tripharmacy.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.tripharmacy.controller.dto.MarkerDto;
import br.com.tripharmacy.controller.dto.ProdutoDto;
import br.com.tripharmacy.controller.form.ProdutoForm;
import br.com.tripharmacy.modelo.Farmacia;
import br.com.tripharmacy.modelo.Produto;
import br.com.tripharmacy.repository.FarmaciaRepository;
import br.com.tripharmacy.repository.ProdutoRepository;

@RestController
@RequestMapping("/produto")
public class ProdutoController {

	@Autowired
	private ProdutoRepository produtoRepository;
	@Autowired
	private FarmaciaRepository farmaciaRepository;

	@GetMapping
	public List<ProdutoDto> lista() {
		List<Produto> produtos = produtoRepository.findAll();
		return ProdutoDto.converter(produtos);
	}

	@PostMapping
	@Transactional
	public ResponseEntity<?> Cadastrar(@RequestBody ProdutoForm form, UriComponentsBuilder uriBilBuilder) {
		if (form.getFarmacia().getIdFarmacia() == 0) {
			return null;
		}
		Farmacia farmacia = farmaciaRepository.findById(form.getFarmacia().getIdFarmacia()).get();
		form.setFarmacia(farmacia);
		Produto produto = form.converter();
		produtoRepository.save(produto);
		URI uri = uriBilBuilder.path("/produto/{id}").buildAndExpand(produto.getIdProduto()).toUri();
		return ResponseEntity.created(uri).body(new ProdutoDto(produto));
	}

	@GetMapping("/{id}")
	public ResponseEntity<ProdutoDto> detalhar(@PathVariable int id) {
		Optional<Produto> produto = produtoRepository.findById(id);
		if (produto.isPresent()) {
			return ResponseEntity.ok(new ProdutoDto(produto.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<ProdutoDto> atualizar(@PathVariable int id, @RequestBody @Valid ProdutoForm form) {
		Optional<Produto> optional = produtoRepository.findById(id);
		if (optional.isPresent()) {
			Produto produto = form.atualizar(id, produtoRepository);
			return ResponseEntity.ok(new ProdutoDto(produto));
		}

		return ResponseEntity.notFound().build();

	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<MarkerDto> remover(@PathVariable int id) {
		Optional<Produto> optional = produtoRepository.findById(id);
		if (optional.isPresent()) {
			produtoRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}

		return ResponseEntity.notFound().build();

	}
}

package br.com.tripharmacy.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.tripharmacy.controller.dto.FarmaciaDto;
import br.com.tripharmacy.controller.form.FarmaciaForm;
import br.com.tripharmacy.modelo.Farmacia;
import br.com.tripharmacy.modelo.Marker;
import br.com.tripharmacy.repository.FarmaciaRepository;
import br.com.tripharmacy.repository.MarkerRepository;

@RestController
@RequestMapping("/farmacia")
public class FarmaciaController {

	@Autowired
	private FarmaciaRepository farmaciaRepository;

	@Autowired
	private MarkerRepository markerRepository;

	@GetMapping
	public List<FarmaciaDto> lista() {
		List<Farmacia> farmacias = farmaciaRepository.findAll();

		return FarmaciaDto.converter(farmacias);
	}

	@PostMapping
	@Transactional
	public ResponseEntity<?> save(@RequestBody FarmaciaForm form, UriComponentsBuilder uriBilBuilder) {
		Optional<Farmacia> farmacia = farmaciaRepository.findByNoCnpj(form.getNoCnpj());

		if (farmacia.isPresent()) {
			return ResponseEntity.status(409).body("{\"MSG\":\"Farmacia já cadastrada.\"}");
		}

		if (form.getMarker().getId() != 0) {
			Marker marker = markerRepository.findByIdMarker(form.getMarker().getId()).get();
			form.setMarker(marker);
		}

		Farmacia farmaciaObjeto = form.converter(form.getMarker());

		farmaciaRepository.save(farmaciaObjeto);
		URI uri = uriBilBuilder.path("/cliente/{noCpfCnpj}").buildAndExpand(farmaciaObjeto.getNoCnpj()).toUri();
		return ResponseEntity.created(uri).body(new FarmaciaDto(farmaciaObjeto));
	};

	@GetMapping("/{noCnpj}")
	@Transactional
	public ResponseEntity<FarmaciaDto> detalhar(@PathVariable String noCnpj) {
		Optional<Farmacia> farmacia = farmaciaRepository.findByNoCnpj(noCnpj);
		if (farmacia.isPresent()) {
			return ResponseEntity.ok(new FarmaciaDto(farmacia.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@PutMapping("/{noCnpj}")
	@Transactional
	public ResponseEntity<FarmaciaDto> atualizar(@PathVariable String noCnpj, @RequestBody @Valid FarmaciaForm form) {
		Optional<Farmacia> optional = farmaciaRepository.findByNoCnpj(noCnpj);

		if (form.getMarker().getId() != 0) {
			Marker marker = markerRepository.findByIdMarker(form.getMarker().getId()).get();
			form.setMarker(marker);
		}
		if (optional.isPresent()) {
			Farmacia farmacia = form.atualizar(optional.get().getIdFarmacia(), farmaciaRepository);
			return ResponseEntity.ok(new FarmaciaDto(farmacia));
		}

		return ResponseEntity.notFound().build();

	}

	@DeleteMapping("/{noCnpj}")
	@Transactional
	public ResponseEntity<FarmaciaDto> remover(@PathVariable String noCnpj) {
		Optional<Farmacia> optional = farmaciaRepository.findByNoCnpj(noCnpj);
		if (optional.isPresent()) {
			farmaciaRepository.deleteById(optional.get().getIdFarmacia());
			return ResponseEntity.ok().build();
		}

		return ResponseEntity.notFound().build();

	}
}

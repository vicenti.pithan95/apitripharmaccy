package br.com.tripharmacy.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.tripharmacy.controller.dto.MarkerDto;
import br.com.tripharmacy.controller.form.MarkerForm;
import br.com.tripharmacy.modelo.Marker;
import br.com.tripharmacy.repository.FarmaciaRepository;
import br.com.tripharmacy.repository.MarkerRepository;

@RestController
@RequestMapping("/marker")
public class MarkerController {

	@Autowired
	private MarkerRepository markerRepository;
	
	@Autowired
	private FarmaciaRepository farmaciaRepository;

	@GetMapping
	public List<MarkerDto> lista() {
		List<Marker> markers = markerRepository.findAll();

		return MarkerDto.converter(markers);
	}

	@PostMapping
	@Transactional
	public ResponseEntity<MarkerDto> Cadastrar(@RequestBody @Valid MarkerForm form,
			UriComponentsBuilder uriBilBuilder) {
		Marker marker = form.converter(farmaciaRepository);
		markerRepository.save(marker);
		URI uri = uriBilBuilder.path("/marker/{id}").buildAndExpand(marker.getId()).toUri();
		return ResponseEntity.created(uri).body(new MarkerDto(marker));
	}

	@GetMapping("/{id}")
	public ResponseEntity<MarkerDto> detalhar(@PathVariable int id) {
		Optional<Marker> marker = markerRepository.findById(id);
		if (marker.isPresent()) {
			return ResponseEntity.ok(new MarkerDto(marker.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<MarkerDto> atualizar(@PathVariable int id, @RequestBody @Valid MarkerForm form) {
		Optional<Marker> optional = markerRepository.findById(id);
		if (optional.isPresent()) {
			Marker marker = form.atualizar(id, markerRepository, farmaciaRepository);
			return ResponseEntity.ok(new MarkerDto(marker));
		}

		return ResponseEntity.notFound().build();

	}
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<MarkerDto> remover(@PathVariable int id) {
		Optional<Marker> optional = markerRepository.findById(id);
		if (optional.isPresent()) {
			markerRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		
		return ResponseEntity.notFound().build();

	}
}

package br.com.tripharmacy.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.tripharmacy.controller.dto.SolicitacaoCompraDto;
import br.com.tripharmacy.controller.form.SolicitacaoCompraForm;
import br.com.tripharmacy.modelo.ItemPedido;
import br.com.tripharmacy.modelo.Produto;
import br.com.tripharmacy.modelo.SolicitacaoCompra;
import br.com.tripharmacy.modelo.Usuario;
import br.com.tripharmacy.repository.ItemRepository;
import br.com.tripharmacy.repository.ProdutoRepository;
import br.com.tripharmacy.repository.SolicitacaoCompraReoisitory;
import br.com.tripharmacy.repository.UsuarioRepository;

@RestController
@RequestMapping("/solicitacaoCompra")
public class SolicitacaoCompraController {

	@Autowired
	private SolicitacaoCompraReoisitory solicitacaoCompraReoisitory;
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	

	@GetMapping
	public List<SolicitacaoCompraDto> lista() {
		List<SolicitacaoCompra> solicitacoes = solicitacaoCompraReoisitory.findAll();

		return SolicitacaoCompraDto.converter(solicitacoes);
	}

	@PostMapping
	@Transactional
	public ResponseEntity<SolicitacaoCompraDto> Cadastrar(@RequestBody @Valid SolicitacaoCompraForm form,
			UriComponentsBuilder uriBilBuilder) {
Usuario usuario = usuarioRepository.findById(form.getUsuario().getIdUsuario()).get();
		
		form.setUsuario(usuario);
		
		SolicitacaoCompra solicitacao = form.converter();

		solicitacaoCompraReoisitory.save(solicitacao);
		
		for (ItemPedido item : form.getItemPedido()) {
			Produto produto = produtoRepository.findById(item.getProduto().getIdProduto()).get();
			item.setProduto(produto);
			item.setSolicitacaoCompra(solicitacao);
			itemRepository.save(item);
		}
		
		
		

		URI uri = uriBilBuilder.path("/solicitacaoCompra/{id}").buildAndExpand(solicitacao.getIdSolicitacao()).toUri();
		return ResponseEntity.created(uri).body(new SolicitacaoCompraDto(solicitacao));
	}

	@GetMapping("/{id}")
	public ResponseEntity<SolicitacaoCompraDto> detalhar(@PathVariable int id) {
		Optional<SolicitacaoCompra> solicitacao = solicitacaoCompraReoisitory.findById(id);
		if (solicitacao.isPresent()) {
			return ResponseEntity.ok(new SolicitacaoCompraDto(solicitacao.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<SolicitacaoCompraDto> atualizar(@PathVariable int id,
			@RequestBody @Valid SolicitacaoCompraForm form) {
		Optional<SolicitacaoCompra> optional = solicitacaoCompraReoisitory.findById(id);
		if (optional.isPresent()) {
			for (ItemPedido item : form.getItemPedido()) {
				Produto produto = produtoRepository.findById(item.getProduto().getIdProduto()).get();
				item.setProduto(produto);
				item.setSolicitacaoCompra(optional.get());
			}
			
			Usuario usuario = usuarioRepository.findById(form.getUsuario().getIdUsuario()).get();
			
			form.setUsuario(usuario);
			SolicitacaoCompra solicitacao = form.atualizar(id, solicitacaoCompraReoisitory, produtoRepository);
			return ResponseEntity.ok(new SolicitacaoCompraDto(solicitacao));
		}

		return ResponseEntity.notFound().build();

	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<SolicitacaoCompraDto> remover(@PathVariable int id) {
		Optional<SolicitacaoCompra> optional = solicitacaoCompraReoisitory.findById(id);
		if (optional.isPresent()) {
			solicitacaoCompraReoisitory.deleteById(id);
			return ResponseEntity.ok().build();
		}

		return ResponseEntity.notFound().build();

	}
}

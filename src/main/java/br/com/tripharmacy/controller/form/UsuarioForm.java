package br.com.tripharmacy.controller.form;

import java.util.Date;

import br.com.tripharmacy.modelo.Usuario;
import br.com.tripharmacy.repository.UsuarioRepository;
import resources.TipoPessoa;

public class UsuarioForm {

	private Long idUsuario;
	private String dsNome;
	private String dsLogin;
	private String tpUsuario;
	private String dsSenha;
	private TipoPessoa tpPessoa;
	private String noCpfCnpj;
	private String noTelefoneCel;
	private String noTelefoneRes;
	private String noTelefoneAux;
	private String dsEmail;
	private Date dtInc;
	private Date dtAlt;

	public Usuario converter(TipoPessoa TipoPessoa) {
		if(tpPessoa == null) {
			return null;
		}
		return new Usuario(idUsuario,dsNome,dsLogin, tpUsuario, dsSenha, tpPessoa,
				 noCpfCnpj, noTelefoneCel,  noTelefoneRes,  noTelefoneAux,  dsEmail,
				 dtInc);
	}

	public Usuario atualizar(Long id, UsuarioRepository usuarioRepository) {
		Usuario usuario = usuarioRepository.getOne(id);
		usuario.setDsLogin(this.dsLogin);
		usuario.setDsSenha(this.dsSenha);
		usuario.setDsNome(this.dsNome);
		usuario.setTpUsuario(this.tpUsuario);
		usuario.setDsEmail(this.dsEmail);
		usuario.setTpPessoa(this.tpPessoa.toString());
		usuario.setNoTelefoneAux(this.noTelefoneAux);
		usuario.setNoTelefoneCel(this.noTelefoneCel);
		usuario.setNoTelefoneRes(this.noTelefoneRes);
		usuario.setDtAlt(new Date(System.currentTimeMillis()));
		return usuario;

	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getDsNome() {
		return dsNome;
	}

	public void setDsNome(String dsNome) {
		this.dsNome = dsNome;
	}

	public String getDsLogin() {
		return dsLogin;
	}

	public void setDsLogin(String dsLogin) {
		this.dsLogin = dsLogin;
	}

	public String getTpUsuario() {
		return tpUsuario;
	}

	public void setTpUsuario(String tpUsuario) {
		this.tpUsuario = tpUsuario;
	}

	public String getDsSenha() {
		return dsSenha;
	}

	public void setDsSenha(String dsSenha) {
		this.dsSenha = dsSenha;
	}

	public TipoPessoa getTpPessoa() {
		return tpPessoa;
	}

	public void setTpPessoa(TipoPessoa tpPessoa) {
		this.tpPessoa = tpPessoa;
	}

	public String getNoCpfCnpj() {
		return noCpfCnpj;
	}

	public void setNoCpfCnpj(String noCpfCnpj) {
		this.noCpfCnpj = noCpfCnpj;
	}

	public String getNoTelefoneCel() {
		return noTelefoneCel;
	}

	public void setNoTelefoneCel(String noTelefoneCel) {
		this.noTelefoneCel = noTelefoneCel;
	}

	public String getNoTelefoneRes() {
		return noTelefoneRes;
	}

	public void setNoTelefoneRes(String noTelefoneRes) {
		this.noTelefoneRes = noTelefoneRes;
	}

	public String getNoTelefoneAux() {
		return noTelefoneAux;
	}

	public void setNoTelefoneAux(String noTelefoneAux) {
		this.noTelefoneAux = noTelefoneAux;
	}

	public String getDsEmail() {
		return dsEmail;
	}

	public void setDsEmail(String dsEmail) {
		this.dsEmail = dsEmail;
	}

	public Date getDtInc() {
		return dtInc;
	}

	public void setDtInc(Date dtInc) {
		this.dtInc = dtInc;
	}

	public Date getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(Date dtAlt) {
		this.dtAlt = dtAlt;
	}

	
	


}

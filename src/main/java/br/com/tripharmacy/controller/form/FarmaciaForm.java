package br.com.tripharmacy.controller.form;

import java.util.Date;

import br.com.tripharmacy.modelo.Farmacia;
import br.com.tripharmacy.modelo.Marker;
import br.com.tripharmacy.repository.FarmaciaRepository;
import br.com.tripharmacy.repository.MarkerRepository;

public class FarmaciaForm {

	MarkerRepository markerRepository;

	private int idFarmacia;
	private Marker marker;
	private String dsFarmacia;
	private String noCnpj;
	private String dsRazaoSocial;

	public Farmacia converter(Marker marker) {
		if (marker == null) {
			return null;
		}
		return new Farmacia(idFarmacia, marker, dsFarmacia, noCnpj, dsRazaoSocial);
	}

	public Farmacia atualizar(int id, FarmaciaRepository farmaciaRepository) {
		Farmacia farmacia = farmaciaRepository.findByidFarmacia(id);
		farmacia.setNoCnpj(this.noCnpj);
		farmacia.setDsFarmacia(this.dsFarmacia);
		farmacia.setMarker(this.marker);
		farmacia.setDsRazaoSocial(this.dsRazaoSocial);
		farmacia.setDtAlt(new Date(System.currentTimeMillis()));
		return farmacia;
	}

	public int getIdFarmacia() {
		return idFarmacia;
	}

	public void setIdFarmacia(int idFarmacia) {
		this.idFarmacia = idFarmacia;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public String getDsFarmacia() {
		return dsFarmacia;
	}

	public void setDsFarmacia(String dsFarmacia) {
		this.dsFarmacia = dsFarmacia;
	}

	public String getNoCnpj() {
		return noCnpj;
	}

	public void setNoCnpj(String noCnpj) {
		this.noCnpj = noCnpj;
	}

	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}


}

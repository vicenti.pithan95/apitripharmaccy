package br.com.tripharmacy.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.tripharmacy.modelo.Farmacia;
import br.com.tripharmacy.modelo.Marker;
import br.com.tripharmacy.repository.FarmaciaRepository;
import br.com.tripharmacy.repository.MarkerRepository;

public class MarkerForm {
	@NotNull
	@NotEmpty
	private String title;
	@NotNull
	private double lat;
	@NotNull
	private double lng;
	@NotNull
	@NotEmpty
	private String snippet;
	@NotNull
	private int idFarmacia;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}

	public int getFarmacia() {
		return idFarmacia;
	}

	public void setIdfarmacia(int idFarmacia) {
		this.idFarmacia = idFarmacia;
	}

	public Marker converter(FarmaciaRepository farmaciaRepository) {
		Farmacia farmacia = farmaciaRepository.findByidFarmacia(idFarmacia);
		return new Marker(title, lat, lng, snippet, farmacia);
	}

	public Marker atualizar(int id, MarkerRepository markerRepository, FarmaciaRepository farmaciaRepository ) {
		Farmacia farmacia = farmaciaRepository.findByidFarmacia(idFarmacia);
		Marker marker = markerRepository.getOne(id);
		marker.setTitle(this.title);
		marker.setLat(this.lat);
		marker.setLng(this.lng);
		marker.setSnippet(this.snippet);
		marker.setFarmacia(farmacia);
		return marker;

	}
}

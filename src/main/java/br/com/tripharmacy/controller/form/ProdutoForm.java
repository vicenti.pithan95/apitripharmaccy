package br.com.tripharmacy.controller.form;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.tripharmacy.modelo.Farmacia;
import br.com.tripharmacy.modelo.Produto;
import br.com.tripharmacy.repository.ProdutoRepository;

public class ProdutoForm {
	private Farmacia farmacia;
	private String dsProduto;
	private int dsQuantidadeDisp;
	private double vlUnitario;
	private String dsTipo;
	private String dsPeso;
	private int qtUnidades;
	private String dsClassificacao;
	private boolean icPrescricaoMedica;
	private String dsBula;
	
	@Autowired
	ProdutoRepository produtoRepository;

	public Produto converter() {

		return new Produto(farmacia, dsProduto, dsQuantidadeDisp, vlUnitario, dsTipo, dsPeso, qtUnidades,
				dsClassificacao, icPrescricaoMedica, dsBula);
	}

	public Produto atualizar(int id, ProdutoRepository produtoRepository) {
		Produto produto = produtoRepository.findById(id).get();
		produto.setFarmacia(this.farmacia);
		produto.setDsProduto(this.dsProduto);
		produto.setDsQuantidadeDisp(this.dsQuantidadeDisp);
		produto.setDtAlt(new Date(System.currentTimeMillis()));
		produto.setVlUnitario(this.vlUnitario);
		produto.setDsTipo(this.dsTipo);
		produto.setDsPeso(this.dsPeso);
		produto.setQtUnidades(this.qtUnidades);
		produto.setDsClassificacao(this.dsClassificacao);
		produto.setIcPrescricaoMedica(this.icPrescricaoMedica);
		produto.setDsBula(this.dsBula);
		return produto;
	}

	public Farmacia getFarmacia() {
		return farmacia;
	}

	public void setFarmacia(Farmacia farmacia) {
		this.farmacia = farmacia;
	}

	public String getDsProduto() {
		return dsProduto;
	}

	public void setDsProduto(String dsProduto) {
		this.dsProduto = dsProduto;
	}

	public int getDsQuantidadeDisp() {
		return dsQuantidadeDisp;
	}

	public void setDsQuantidadeDisp(int dsQuantidadeDisp) {
		this.dsQuantidadeDisp = dsQuantidadeDisp;
	}

	public double getVlUnitario() {
		return vlUnitario;
	}

	public void setVlUnitario(double vlUnitario) {
		this.vlUnitario = vlUnitario;
	}

	public String getDsTipo() {
		return dsTipo;
	}

	public void setDsTipo(String dsTipo) {
		this.dsTipo = dsTipo;
	}

	public String getDsPeso() {
		return dsPeso;
	}

	public void setDsPeso(String dsPeso) {
		this.dsPeso = dsPeso;
	}

	public int getQtUnidades() {
		return qtUnidades;
	}

	public void setQtUnidades(int qtUnidades) {
		this.qtUnidades = qtUnidades;
	}

	public String getDsClassificacao() {
		return dsClassificacao;
	}

	public void setDsClassificacao(String dsClassificacao) {
		this.dsClassificacao = dsClassificacao;
	}

	public boolean isIcPrescricaoMedica() {
		return icPrescricaoMedica;
	}

	public void setIcPrescricaoMedica(boolean icPrescricaoMedica) {
		this.icPrescricaoMedica = icPrescricaoMedica;
	}

	public String getDsBula() {
		return dsBula;
	}

	public void setDsBula(String dsBula) {
		this.dsBula = dsBula;
	}
	

}

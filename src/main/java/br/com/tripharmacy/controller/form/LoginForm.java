package br.com.tripharmacy.controller.form;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class LoginForm {

	private String dsLogin;
	
	private String dsSenha;
	
	
	
	public String getDsLogin() {
		return dsLogin;
	}
	public String getDsSenha() {
		return dsSenha;
	}
	public void setDsLogin(String dsLogin) {
		this.dsLogin = dsLogin;
	}
	public void setDsSenha(String dsSenha) {
		this.dsSenha = dsSenha;
	}
	public UsernamePasswordAuthenticationToken converter() {
		return new UsernamePasswordAuthenticationToken(dsLogin, dsSenha);
	}

}

package br.com.tripharmacy.controller.form;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import br.com.tripharmacy.modelo.ItemPedido;
import br.com.tripharmacy.modelo.SolicitacaoCompra;
import br.com.tripharmacy.modelo.Usuario;
import br.com.tripharmacy.repository.ProdutoRepository;
import br.com.tripharmacy.repository.SolicitacaoCompraReoisitory;

public class SolicitacaoCompraForm {
	private ProdutoRepository produtoRepository;
	private int idSolicitacao;
	private List<ItemPedido> itemPedido;
	private Usuario usuario;
	private String dsStatus;
	private int dsQuantidade;

	public SolicitacaoCompra converter() {
	
		
		return new SolicitacaoCompra(idSolicitacao, itemPedido, usuario, dsStatus, dsQuantidade);
	}

	public SolicitacaoCompra atualizar(int id, SolicitacaoCompraReoisitory solicitacaoCompraReoisitory,
			ProdutoRepository produtoRepository) {
		Optional<SolicitacaoCompra> solicitacaoCompra = solicitacaoCompraReoisitory.findById(id);
		solicitacaoCompra.get().setUsuario(this.usuario);
		solicitacaoCompra.get().setDtAlt(new Date(System.currentTimeMillis()));
		solicitacaoCompra.get().setDsStatus(this.dsStatus);
		solicitacaoCompra.get().setItemPedido(this.itemPedido);
		return solicitacaoCompra.get();
	}

	public int getIdSolicitacao() {
		return idSolicitacao;
	}

	public void setIdSolicitacao(int idSolicitacao) {
		this.idSolicitacao = idSolicitacao;
	}

	public List<ItemPedido> getItemPedido() {
		return itemPedido;
	}

	public void setItemPedido(List<ItemPedido> itemPedido) {
		this.itemPedido = itemPedido;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getDsStatus() {
		return dsStatus;
	}

	public void setDsStatus(String dsStatus) {
		this.dsStatus = dsStatus;
	}

	public int getDsQuantidade() {
		return dsQuantidade;
	}

	public void setDsQuantidade(int dsQuantidade) {
		this.dsQuantidade = dsQuantidade;
	}

}
